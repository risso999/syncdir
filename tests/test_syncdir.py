import filecmp
from datetime import datetime
from pathlib import Path
from typing import List

import pytest

from syncdirs.syncdir import SyncDir


def list_dir(dir: Path) -> List[str]:
    """
    The function recursively lists all files and directories in a given
    directory.

    Args:
        dir (Path): The `dir` parameter is expected to be a `Path` object,
        which represents a directory path.
    """
    result = []
    for item in dir.iterdir():
        result.append(item.name)
        if item.is_dir():
            items = [
                str(Path(item.name) / Path(subitem))
                for subitem in list_dir(item)
            ]
            result += items
    return result


class TestSyncDir:
    @pytest.fixture
    def temp_source_path(self, tmp_path: Path) -> str:
        source = tmp_path / "source"
        source.mkdir()
        return str(source)

    @pytest.fixture
    def temp_replica_path(self, tmp_path: Path) -> str:
        replica = tmp_path / "replica"
        replica.mkdir()
        return str(replica)

    @pytest.fixture
    def temp_logfile_path(self, tmp_path: Path) -> str:
        return str(tmp_path / "test.log")

    @pytest.fixture
    def synchronizer(
        self,
        temp_source_path: str,
        temp_replica_path: str,
        temp_logfile_path: str,
    ) -> SyncDir:
        return SyncDir(temp_source_path, temp_replica_path, temp_logfile_path)

    def test_replica_is_subdir(
        self, temp_source_path: str, temp_logfile_path: str
    ) -> None:
        """
        Tests fail if replica is a subdirectory.

        Args:
            temp_source_path (str): The path of source testing folder
            temp_logfile_path (str): The path of testing logfile
        """
        with pytest.raises(ValueError) as exc_info:
            SyncDir(
                temp_source_path,
                str(Path(temp_source_path) / "subdir"),
                temp_logfile_path,
            )
        assert str(exc_info.value) == "Replica cannot be a subdirectory!"

    def test_sync_last_sync_updated(self, synchronizer: SyncDir) -> None:
        """
        Tests whether the `last_sync` attribute of a `SyncDir` object is
        updated after calling the `sync` method.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        now = datetime.now()
        synchronizer.sync()
        assert synchronizer.last_sync
        assert synchronizer.last_sync > now

    def test_is_modified_no_last_sync(self, synchronizer: SyncDir) -> None:
        """
        Tests if method `is_modified` returns True if no last sync.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        assert synchronizer.is_modified(
            synchronizer.source, synchronizer.replica
        )

    def test_is_modified_not_modified_since_last_sync(
        self, synchronizer: SyncDir
    ) -> None:
        """
        Tests if method `is_modified` returns False if not modified since last
        sync.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        synchronizer.sync()
        assert not synchronizer.is_modified(
            synchronizer.source, synchronizer.replica
        )

    def test_is_modified_modified_source_dir(
        self, synchronizer: SyncDir
    ) -> None:
        """
        Tests if method `is_modified` returns True if modified source dir.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        synchronizer.sync()
        synchronizer.remove(synchronizer.source)
        assert synchronizer.is_modified(
            synchronizer.source, synchronizer.replica
        )

    def test_is_modified_modified_replica_dir(
        self, synchronizer: SyncDir
    ) -> None:
        """
        Tests if method `is_modified` returns True if modified replica dir.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        synchronizer.sync()
        synchronizer.remove(synchronizer.replica)
        assert synchronizer.is_modified(
            synchronizer.source, synchronizer.replica
        )

    def test_remove_file(self, synchronizer: SyncDir) -> None:
        """
        Tests if file is removed.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        new_file = synchronizer.replica / "test.txt"
        new_file.touch()
        assert new_file.is_file()
        synchronizer.remove(new_file)
        assert not new_file.exists()

    def test_remove_dir(self, synchronizer: SyncDir) -> None:
        """
        Tests if dir is removed.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        new_dir = synchronizer.replica / "test"
        new_dir.mkdir()
        new_file = new_dir / "test.txt"
        new_file.touch()
        assert new_dir.is_dir()
        assert new_file.is_file()
        synchronizer.remove(new_dir)
        assert not new_file.exists()
        assert not new_dir.exists()

    def test_remove_not_exist(self, synchronizer: SyncDir) -> None:
        """
        Tests if it won't fail if dir does not exist.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        path_not_exist = synchronizer.replica / "test"
        assert not path_not_exist.exists()
        synchronizer.remove(path_not_exist)
        assert not path_not_exist.exists()

    def test_copy_file(self, synchronizer: SyncDir) -> None:
        """
        Tests if file is copied.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        source_file = synchronizer.source / "test.txt"
        source_file.write_text("source")
        replica_file = synchronizer.replica / "test.txt"
        replica_file.write_text("replica")
        assert not filecmp.cmp(source_file, replica_file)
        synchronizer.copy(source_file, replica_file)
        assert filecmp.cmp(source_file, replica_file)

    def test_copy_dir(self, synchronizer: SyncDir) -> None:
        """
        Tests if dir is copied.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        source_file = synchronizer.source / "test.txt"
        source_file.touch()
        replica_file = synchronizer.replica / "test.txt"
        assert source_file.is_file()
        assert not replica_file.exists()
        synchronizer.copy(synchronizer.source, synchronizer.replica)
        assert filecmp.cmp(source_file, replica_file)

    def test_copy_not_exist(self, synchronizer: SyncDir) -> None:
        """
        Tests if replica file is removed if source file not exist.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        source_file = synchronizer.source / "test.txt"
        replica_file = synchronizer.replica / "test.txt"
        replica_file.touch()
        assert not source_file.exists()
        assert replica_file.is_file()
        synchronizer.copy(synchronizer.source, synchronizer.replica)
        assert not source_file.exists()
        assert not replica_file.exists()

    def test_sync(self, synchronizer: SyncDir) -> None:
        """
        Tests if sync works properly.

        Args:
            synchronizer (SyncDir): The new SyncDir instance
        """
        # update source directory
        source_file1 = synchronizer.source / "test1.txt"
        source_file1.touch()
        source_dir = synchronizer.source / "test1"
        source_dir.mkdir()
        synchronizer.copy(synchronizer.source, synchronizer.source / "test2")
        source_file2 = synchronizer.source / "test2.txt"
        source_file2.write_text("same")
        # update replica directory
        synchronizer.copy(source_file2, synchronizer.replica / "test2.txt")
        replica_file = synchronizer.replica / "test3.txt"
        replica_file.touch()
        assert list_dir(synchronizer.source) != list_dir(synchronizer.replica)
        synchronizer.sync()
        assert list_dir(synchronizer.source) == list_dir(synchronizer.replica)
        assert filecmp.cmp(
            synchronizer.source / "test1.txt",
            synchronizer.replica / "test1.txt",
        )
        assert filecmp.cmp(
            synchronizer.source / "test2" / "test1.txt",
            synchronizer.replica / "test2" / "test1.txt",
        )
        synchronizer.sync()
        assert list_dir(synchronizer.source) == list_dir(synchronizer.replica)
