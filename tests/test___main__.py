import argparse

import pytest

from syncdirs.__main__ import interval, parse_args


class TestSyncDirMain:
    SOURCE = "source"
    REPLICA = "replica"
    LOGFILE = "test.log"
    INTERVAL = "1"
    ARGS = [
        "syncdirs",
        "--logfile",
        LOGFILE,
        "--interval",
        INTERVAL,
        SOURCE,
        REPLICA,
    ]

    def test_interval(self) -> None:
        """
        The function `test_interval` tests the `interval` function by asserting
        that it returns the integer value of a given string.
        """
        assert interval("123") == 123

    def test_interval_not_positive(self) -> None:
        """
        The function `test_interval_not_positive` tests if the `interval`
        function raises an `argparse.ArgumentTypeError` with the correct error
        message when given a non-positive integer as input.
        """
        with pytest.raises(argparse.ArgumentTypeError) as exc_info:
            interval("0")
        assert str(exc_info.value) == "0 is not a positive integer"

    def test_interval_not_integer(self) -> None:
        """
        The function `test_interval_not_integer` tests if the `interval`
        function raises an `argparse.ArgumentTypeError` with the correct error
        message when given a non-integer argument.
        """
        with pytest.raises(argparse.ArgumentTypeError) as exc_info:
            interval("test")
        assert str(exc_info.value) == "test is not a valid integer"

    def test_parse_args(self, monkeypatch: pytest.MonkeyPatch):
        """
        The function `test_parse_args` tests the `parse_args` function by
        setting command line arguments.

        Args:
            monkeypatch (pytest.MonkeyPatch): The `monkeypatch` parameter is an
        instance of the `pytest.MonkeyPatch` class. It is used to modify the
        behavior of the `sys.argv` attribute, which represents the command-line
        arguments passed to the script.
        """
        monkeypatch.setattr("sys.argv", self.ARGS)
        result_args = parse_args()
        assert result_args.SOURCE == self.SOURCE
        assert result_args.REPLICA == self.REPLICA
        assert result_args.logfile == self.LOGFILE
        assert result_args.interval == int(self.INTERVAL)
